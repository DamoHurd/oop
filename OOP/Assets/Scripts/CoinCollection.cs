﻿using UnityEngine;
using System.Collections;

public class CoinCollection : MonoBehaviour 
{
	public GameObject coin;
	public Player playa;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Player"))
		{
			Destroy(coin);
			playa.score ++;
		}
	}
}
