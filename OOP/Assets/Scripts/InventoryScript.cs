﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Item
{
	public string name;
	public string description;
	public string useMessage;
}
public class InventoryScript : MonoBehaviour {
	public int currentIndex;
	public Item[] item;
	
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		CheckKeys();
	}
	void CheckKeys()
	{
		if(Input.GetKeyDown(KeyCode.RightBracket))
		{
			IncreaseIndex();
		}
		if(Input.GetKeyDown (KeyCode.LeftBracket))
		{
			DecreaseIndex();
		}
		if(Input.GetKeyDown(KeyCode.Space))
		{
			UseItem();
		}
	}
	void IncreaseIndex()
	{
		currentIndex ++;
		if(currentIndex >  item.Length - 1)
		{
			currentIndex = item.Length - 1;
		}
	}
	void DecreaseIndex()
	{
		currentIndex --;
		if(currentIndex < 0)
		{
			currentIndex = 0;
		}
	}
	void UseItem()
	{
		print(item[currentIndex].useMessage);
	}

}
