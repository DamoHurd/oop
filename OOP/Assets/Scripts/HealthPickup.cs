﻿using UnityEngine;
using System.Collections;

public class HealthPickup : MonoBehaviour 
{
	public int recoverAmount;

	void DestroyPickup()
	{
		Destroy(this.gameObject);
	}

	void addHealth(Player targetPlayer)
	{
		targetPlayer.myStats.health += recoverAmount;
	}

	void OnTriggerEnter(Collider col)
	{
		Player triggeredPlayer = col.gameObject.GetComponent<Player>();
		//!= means not equal to
		if(triggeredPlayer != null)
		{
			addHealth(triggeredPlayer);
			DestroyPickup();
		}
	}

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
