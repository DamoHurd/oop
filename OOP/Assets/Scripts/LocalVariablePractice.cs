﻿using UnityEngine;
using System.Collections;

public class LocalVariablePractice : MonoBehaviour 
{
	public string playerName;
	public string sentence;
	public GameObject lastCollidedObject;

	// Use this for initialization
	void Start () 
	{
		SetPlayerName();
	}
	
	// Update is called once per frame
	void Update () 
	{
		createSentence();
		createDungeonMessage();
	}
	void SetPlayerName()
	{
		string tempName;
		tempName = "Player Two";
		playerName = "Player One";
	}
	void createSentence()
	{
		string sentence = "";
		if(Input.GetKeyDown(KeyCode.Alpha1))
		{
			sentence += ("My name is " + playerName);
		}
		if(Input.GetKeyDown(KeyCode.Alpha2))
		{
			sentence += "\n";
			sentence += "One day I died. The end.";
		}
		if(sentence.Length > 0)
		{
			print(sentence);
		}
	}
	void createDungeonMessage()
	{
		string dungeonMessage;
		dungeonMessage = "";

		if(Input.GetKeyDown(KeyCode.I))
		{
			string message;
			message = "You have entered a dungeon.\n";
			dungeonMessage += message;

		}
		if(Input.GetKeyDown(KeyCode.O))
		{
			string message;
			message = "You see a dragon!\n";
			dungeonMessage += message;
		}
		if(Input.GetKeyDown(KeyCode.P))
		{
			string message;
			message = "You are fucked!\n";
			dungeonMessage += message;
		}
		if(Input.GetKeyDown(KeyCode.U))
		{
			string message;
			message = "You are the champion!\n";
			dungeonMessage += message;
		}
		if(Input.GetKeyDown(KeyCode.Return))
		{
			print(sentence);
			sentence = "";
		}
		if(dungeonMessage.Length > 0)
		{
			dungeonMessage += sentence;

		}
	}

	void OnCollisionEnter(Collision other)
	{
		lastCollidedObject = other.gameObject;
	}

}
