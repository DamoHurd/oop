﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct KeyBindings
{
	public KeyCode moveForward;
	public KeyCode moveLeft;
	public KeyCode moveBack;
	public KeyCode moveRight;
	public KeyCode jump;
}

public class RigidbodyMover : MonoBehaviour 
{

	private Rigidbody rb; 
	public float playerSpeed = 0.5f;
	public float jumpForce = 1f;
	public bool canJump;
	public KeyBindings keyBinds;
	
	void MovePlayer()
	{
		keyBinds.moveForward = KeyCode.W;
		keyBinds.moveLeft = KeyCode.A;
		keyBinds.moveBack = KeyCode.S;
		keyBinds.moveRight = KeyCode.D;
		keyBinds.jump = KeyCode.Space;
	}
	
	// Use this for initialization
	void Start () 
	{
		rb = GetComponent<Rigidbody>();
		jumpForce = 300f;
	}
	
	// Update is called once per frame
	void Update () 
	{
		MovePlayer();
	}

	void FixedUpdate()
	{
		playerInput();
		Speed();
	}

	
	private void playerInput()
	{
		if (Input.GetKey (keyBinds.moveForward))
		{
			rb.AddForce(Vector3.forward * playerSpeed) ;
			print("You just pressed 'W'");		
		}
		if (Input.GetKey(keyBinds.moveLeft))
		{
			rb.AddForce(Vector3.left * playerSpeed);
			print ("You just pressed 'A'");
		}
		if(Input.GetKey (keyBinds.moveBack))
		{		
			rb.AddForce(Vector3.back * playerSpeed);
			print("You just pressed 'S'");		
		}	
		if (Input.GetKey(keyBinds.moveRight))
		{			
			rb.AddForce (Vector3.right * playerSpeed);
			print ("You just pressed 'D'");
		}
		if(Input.GetKeyDown(keyBinds.jump) && canJump)
		{
			canJump = false;
			rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
			print("You just pressed 'Space'");
		}
	}
	
	void OnCollisionEnter (Collision other)
		{
			canJump = true;
		}
	void Speed()
	{
		if (playerSpeed > 20);
			playerSpeed = 20;
	}
	
}
