﻿using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour 
{
	public Rigidbody rb;
	public float playerSpeed = 10f;

	// Use this for initialization
	void Start () 
	{
		rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void FixedUpdate()
	{
		CheckKeys();
	}
	/*
	void MoveForward()
	{
		rb.AddForce(Vector3.forward * playerSpeed);
			
	}
	 void MoveBackward()
	{
		rb.AddForce(Vector3.back * playerSpeed);

	}
	void MoveLeft()
	{
		rb.AddForce(Vector3.left * playerSpeed);
	}

	void MoveRight()
	{
		rb.AddForce(Vector3.right * playerSpeed);
	}
	*/

	void Move(Vector3 direction, float forceAmount)
	{
		rb.AddForce(direction * forceAmount);
	}
	
	void CheckKeys()
	{
		if(Input.GetKey(KeyCode.D))
		{
			print("D is being held, moving right");
			//MoveRight();
			Move (Vector3.right, playerSpeed);
		}
		if(Input.GetKey(KeyCode.A))
		{
			print ("A is being held, moving left");
			//MoveLeft();
			Move (Vector3.left, playerSpeed);
		}
		if(Input.GetKey (KeyCode.S))
		{
			print ("S is being held, moving back");
			//MoveBackward();
			Move (Vector3.back, playerSpeed);
		}
		if(Input.GetKey(KeyCode.W))
		{
			print ("W is being held, moving forward");
			//MoveForward();
			Move (Vector3.forward, playerSpeed);
		}
		if(Input.GetKey(KeyCode.Space))
		{
			Move (Vector3.up, playerSpeed*10);
		}
	}

}