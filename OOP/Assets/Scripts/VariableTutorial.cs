﻿using UnityEngine;
using System.Collections;

public class VariableTutorial : MonoBehaviour {

	public string firstName;
	public string lastName;
	public int age;
	public string dateOfBirth;
	public float money;
	public int currentYear;
	public string fullName;
	public int birthYear; 

	void Start () 
	{
		 
		lastName = "Jenkins";
		age = 17;
		dateOfBirth = "2/11/1997";
		money = 4.20f;
		currentYear = 2015;
		birthYear = currentYear - age;

		print ("My first name is " + firstName);
		print ("My last name is " + lastName);
		print ("I am " + age + " years old");
		print ("I was born on the " + dateOfBirth);
		firstName = "Leroy";
		print ("I have $" + money);
		print ("The current year is " + currentYear);
		print (birthYear);
		fullName = firstName + " " + lastName; 
		print ("My full name is " + fullName);
	}



	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
		    print ("You pressed the spacebar!");
		}
	
	}

}
