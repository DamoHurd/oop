﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour 
{
	public GameObject currentScore;
	private int score = 0;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		currentScore.GetComponent<Text>().text = "Score: " + score;
		ScoreIncrement();
	}

	void ScoreIncrement()
	{
		score += 1;
	}
}
