﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Address
{
	public int houseNumber;
	public string streetName;
	public int postCode;
	public string city;
	public string state;
	public string country;
}

[System.Serializable]
struct KeyBindingz
{
	public KeyCode moveForward;
	public KeyCode moveLeft;
	public KeyCode moveBack;
	public KeyCode moveRight;
	public KeyCode jump;
}

public class StructTutorial : MonoBehaviour 
{
	
	public Address myAddress;
	public KeyBindings keyBinds;
	public Rigidbody rb;
	public float playerSpeed = 0.5f;
	public float jumpForce = 100f;
	public bool canJump;
	
	void MovePlayer()
	{
		keyBinds.moveForward = KeyCode.W;
		keyBinds.moveBack = KeyCode.S;
		keyBinds.moveLeft = KeyCode.A;
		keyBinds.moveRight = KeyCode.D;
		keyBinds.jump = KeyCode.Space;
	}
	// Use this for initialization
	void Start () 
	{
		SetStartingAddress();
		rb = GetComponent<Rigidbody>();
		jumpForce = 300f;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown (KeyCode.Space))
			ChangeAddress();
	}
	
	void FixedUpdate()
	{
		PlayerMovement();
	}
	
	void SetStartingAddress()
	{
		myAddress.houseNumber = 213;
		myAddress.streetName = "Pacific Hwy";
		myAddress.postCode = 2065;
		myAddress.city = "St Leonards";
		myAddress.state = "NSW";
		myAddress.country = "Australia";
	}
	
	void ChangeAddress()
	{
		myAddress.houseNumber = 123;
		myAddress.streetName = "Fake Street";
		myAddress.postCode = 1000;
		myAddress.city = "Springfield";
		myAddress.state = "???";
		myAddress.country = "Merica";
	}
	void PlayerMovement()
	{
		if(Input.GetKey(keyBinds.moveForward))
		{
			rb.AddForce(Vector3.up * playerSpeed); 
		}
		if(Input.GetKey(keyBinds.moveLeft))
		{
			rb.AddForce (Vector3.left * playerSpeed);
		}
		if(Input.GetKey(keyBinds.moveBack))
		{
			rb.AddForce(Vector3.back * playerSpeed);
		}
		if(Input.GetKey(keyBinds.moveRight))
		{
			rb.AddForce(Vector3.right * playerSpeed);
		}
		if(Input.GetKey(keyBinds.jump)&& canJump)
		{
			canJump = false;
			rb.AddForce(Vector3.up * jumpForce);
		}
	
	}
	void OnCollisionEnter (Collision other)
		{
			canJump = true;
		}
	
}
