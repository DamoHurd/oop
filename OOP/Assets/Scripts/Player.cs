﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Stats
{
	public int health;
	public string name;
	public float movementSpeed;
	public float jumpHeight;
	public KeyBindings keyBinds;
}

public class Player : MonoBehaviour 
{
	private Rigidbody rb; 
	public bool canJump;
	public Stats myStats;
	public int score = 0;
	

	// Use this for initialization
	void Start () 
	{
		rb = GetComponent<Rigidbody>();
		MyStatistics();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	void FixedUpdate()
	{
		playerInput();
		
	}
	
	public void playerInput()
	{
		if (Input.GetKey (myStats.keyBinds.moveForward))
		{
			rb.AddForce(Vector3.forward * myStats.movementSpeed) ;
			print("You just pressed 'W'");		
		}
		if (Input.GetKey(myStats.keyBinds.moveLeft))
		{
			rb.AddForce(Vector3.left * myStats.movementSpeed);
			print ("You just pressed 'A'");
		}
		if(Input.GetKey (myStats.keyBinds.moveBack))
		{		
			rb.AddForce(Vector3.back * myStats.movementSpeed);
			print("You just pressed 'S'");		
		}	
		if (Input.GetKey(myStats.keyBinds.moveRight))
		{			
			rb.AddForce (Vector3.right * myStats.movementSpeed);
			print ("You just pressed 'D'");
		}
		if(Input.GetKeyDown(myStats.keyBinds.jump) && canJump)
		{
			canJump = false;
			rb.AddForce(Vector3.up * myStats.jumpHeight, ForceMode.Impulse);
			print("You just pressed 'Space'");
		}
	}
	void OnCollisionEnter (Collision other)
	{
		canJump = true;
	}
	/*void Speed()
	{
		if (myStats.movementSpeed > 20);
		myStats.movementSpeed = 20;
	}
*/
	
	private void MyStatistics(){
		myStats.health = 100;
		myStats.name = "SmirkyLemonChops";
		myStats.movementSpeed = 20f;
		myStats.jumpHeight = 5f;
		myStats.keyBinds.moveForward = KeyCode.W;
		myStats.keyBinds.moveBack = KeyCode.S;
		myStats.keyBinds.moveLeft = KeyCode.A;
		myStats.keyBinds.moveRight = KeyCode.D;
		myStats.keyBinds.jump = KeyCode.Space;
	}

}


