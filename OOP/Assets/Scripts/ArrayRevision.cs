﻿using UnityEngine;
using System.Collections;

public class ArrayRevision : MonoBehaviour {

	public int[] collectiveAges;
	public int arraySize = 5;
	public int currentIndex;
	
	// Use this for initialization
	void Start () 
	{
		collectiveAges = new int[arraySize];
		SetInitialValues(); 
	}
	
	// Update is called once per frame
	void Update () 
	{
		CheckKeys();
	}
	
	void SetInitialValues()
	{
		for(int i = 0; i < arraySize; i++)
			{
				collectiveAges[i] = -1;
				print(collectiveAges[i]);
			}
	}
	void CheckKeys()
	{
		if(Input.GetKeyDown(KeyCode.UpArrow))
		{
			IncrementIndex();
		}
		if(Input.GetKeyDown(KeyCode.DownArrow))
		{
			DecreaseIndex();
		}
		if(Input.GetKeyDown(KeyCode.Return))
			{
				print("Enter Pressed!");
				SetAge();
			}
	}
	void SetAge()
	{
		collectiveAges[currentIndex] = 20;
	}
	void DecreaseIndex()
	{
		currentIndex -= 1;
		
		if(currentIndex < 0)
		{
			currentIndex = 0;
			Debug.LogWarning("Negative numbers are out of range! CurrentIndex has been set to 0.");
		}
	}
	void IncrementIndex()
	{
		currentIndex += 1;
		
		if(currentIndex > arraySize - 1)
		{
			currentIndex = arraySize - 1;
			Debug.LogWarning("Too high! Numbers must be inside range!");
		} 
	}
}