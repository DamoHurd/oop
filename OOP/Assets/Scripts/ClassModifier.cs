﻿using UnityEngine;
using System.Collections;

public class ClassModifier : MonoBehaviour 
{
	public ClassPractice classPrac;
	// Use this for initialization
	void Start () 
	{
		classPrac.printBool();
		classPrac.printDifferentWord();
		classPrac.printFloat();
		classPrac.printNumber();
		classPrac.printWord();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
