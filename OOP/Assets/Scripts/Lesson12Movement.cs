﻿using UnityEngine;
using System.Collections;

public class Lesson12Movement : MonoBehaviour 
{
	public float playerSpeed = 10;
	private Rigidbody rb;
	public bool canJump;
	public float jumpSpeed;
	public GameObject Platform;
	public Transform spawnPoint;
	public float maxJump = 9;

	// Use this for initialization
	void Start () 
	{
		rb = gameObject.GetComponent<Rigidbody>();

	}

	
	// Update is called once per frame
	void Update () 
	{
		rb.transform.Translate(Vector3.back * playerSpeed * Time.deltaTime);
		if(jumpSpeed > maxJump)
		{
			jumpSpeed = maxJump;
		}
		playerSpeed += Time.deltaTime;
	}

	void FixedUpdate()
	{
		LeftRight();
		Jump();
	}
	
	void LeftRight()
	{
		if(Input.GetKey(KeyCode.A))
		{
			rb.transform.Translate(Vector3.right * playerSpeed * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.D))
		{
			rb.transform.Translate(Vector3.left * playerSpeed * Time.deltaTime);
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if(other.CompareTag("Obstacle"))
		{
			playerSpeed -= 0.5f;
			Destroy(other.gameObject);
		}

		if(other.CompareTag("DeathZone"))
		{
			Application.LoadLevel(0);
		}
	}

	void Jump()
	{
		if(Input.GetKey(KeyCode.Space))
		{
			jumpSpeed += Time.deltaTime * 3f;
		}

		if(Input.GetKeyUp(KeyCode.Space) && canJump)
		{
			canJump = false;
			rb.AddForce(Vector3.up * jumpSpeed, ForceMode.Impulse);
		}

	}

	void OnCollisionEnter (Collision other)
	{
		canJump = true;
	}
	

}
