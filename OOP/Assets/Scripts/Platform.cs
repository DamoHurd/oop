﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour 
{
	public GameObject platform;
	public Lesson12Movement script;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void SpawnPlatform()
	{
		if(script.playerSpeed <= 10)
		{
			Vector3 spawnPos = new Vector3(transform.position.x, transform.position.y, transform.position.z - 60);
			Instantiate(platform, spawnPos, Quaternion.identity);
		}
		if(script.playerSpeed <= 5)
		{
			Vector3 spawnPos = new Vector3(transform.position.x, transform.position.y, transform.position.z - 55);
			Instantiate(platform, spawnPos, Quaternion.identity);
		}
	}
	void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Player"))
		{
			SpawnPlatform();
		}
	}
}
