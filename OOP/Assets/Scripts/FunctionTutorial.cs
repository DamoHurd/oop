﻿using UnityEngine;
using System.Collections;

public class FunctionTutorial : MonoBehaviour 
{
	public string myName = "Damian Hurd";
	public int myAge = 17;
	public bool male;
	public bool female; 
	public string randomSentence = "FUCK YOU ASSHOLE";
	public float playerSpeed = 0.5f;



	void Start () 
	{
		printMyName();
		printID();
	}
	

	void Update () 
	{
		checkKeyPresses();
		movePlayerForward();
		movePlayerRight();
		movePlayerLeft();
		movePlayerBackwards();
		Jump();

		
	}

	public void printMyName()
	{
		print ("Damian");
	}

	public void printID()
	{
		print ("Name: " + myName);
		print ("Age: " + myAge);
		if (male == true)
		{
			print("Sex: Male");
		}
		else
		{
			print ("Sex: Female");
		}
		print (randomSentence);

	
	}

	public void checkKeyPresses()
	{
		if (Input.GetKeyDown(KeyCode.W))
			print ("You just pressed 'W'");
		if (Input.GetKeyDown(KeyCode.A))
			print ("You just pressed 'A'");
		if (Input.GetKeyDown(KeyCode.S))
			print ("You just pressed 'S'");
			movePlayerBackwards();
		if (Input.GetKeyDown(KeyCode.D))
			print ("You just pressed 'D'");
	
	}

	public void movePlayerForward()
	{
		if (Input.GetKey(KeyCode.W))
			transform.Translate (Vector3.forward * Time.deltaTime);
	}
	public void movePlayerBackwards()
	{
		if (Input.GetKey(KeyCode.S))
			transform.Translate (Vector3.back * Time.deltaTime);
	}
	public void movePlayerLeft()
	{
		if(Input.GetKey(KeyCode.A))
			transform.Translate (Vector3.left * Time.deltaTime);
	}
	public void movePlayerRight()
	{
		if(Input.GetKey(KeyCode.D))
			transform.Translate(Vector3.right * Time.deltaTime);
	}
	public void Jump()
	{
		if (Input.GetKey(KeyCode.Space))
			transform.Translate(Vector3.up * Time.deltaTime); 
	}



}
